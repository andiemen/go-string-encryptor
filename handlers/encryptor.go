package handlers

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type Encryption struct {
	Input string `json:"input"`
	Output []byte `json:"output"`
	Error error `json:"error,omitempty"`
}

func Encrypt(key, text []byte) Encryption {
	block, err := aes.NewCipher(key)
	fmt.Printf("%s\n%s\n", key, text)
	if err != nil {
		encryption := Encryption{
			Input: string(text),
			Output: []byte("nn"),
			Error: err,

		}
		return encryption
	}
	b := base64.StdEncoding.EncodeToString(text)
	cipherText := make([]byte, aes.BlockSize+len(b))
	iv := cipherText[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		encryption := Encryption{
			Input: string(text),
			Output: []byte("nnn"),
			Error: err,

		}
		return encryption
	}
	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(cipherText[aes.BlockSize:], []byte(b))
	encryption := Encryption{
		Input:  string(text),
		Output: cipherText,
		Error:  nil,
	}
	fmt.Printf("\n%s\n", encryption)
	return encryption
}

func (e *Encryption) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	key := []byte("secret 32 byte key (kp in vault)")
	text, ok := r.URL.Query()["text"]
	if !ok || len(text[0]) < 1 {
		http.Error(rw, "Oops... No text found", http.StatusBadRequest)
		return
	}
	json.NewEncoder(rw).Encode(Encrypt(key, []byte(text[0])))
}

