# Go string encryptor

In this exercise you will need to build an encrypted string generator. This will be done with two microservices written in GO. 
This exercise comes to test the ability to write microservices on cloud environments, creating API and to handle goroutine

After cloning, run 
- "docker build -t golang-string-encryptor-api ."
- "docker run -p 8080:9090 golang-string-encryptor-api"